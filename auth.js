var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bodyParser = require('body-parser'),
    session = require('express-session');

// hardcoded users, ideally the users should be stored in a database
var users = [{
    "id": 111,
    "username": "admin",
    "password": "k4v5b6"
}];

function initPassport(app) {
    // passport needs ability to serialize and unserialize users out of session
    passport.serializeUser(function(user, done) {
        done(null, users[0].id);
    });

    passport.deserializeUser(function(id, done) {
        done(null, users[0]);
    });

    // passport local strategy for local-login, local refers to this app
    passport.use('local-login', new LocalStrategy(
        function(username, password, done) {
            if (username === users[0].username && password === users[0].password) {
                return done(null, users[0]);
            } else {
                return done(null, false, {
                    "message": "User not found."
                });
            }
        }));

    // body-parser for retrieving form data
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // initialize passport and and session for persistent login sessions
    app.use(session({
        secret: "tHiSiSasEcRetStr",
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());

    // route middleware to ensure user is logged in
    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();

        // res.sendStatus(401);
        res.redirect("/login.html");
    }

    // define login
    app.post("/login",
        passport.authenticate("local-login", {
            failureRedirect: "/login.html"
        }),
        function(req, res) {
            res.redirect("/");
        });

    // define logout
    app.get("/logout", function(req, res) {
        req.logout();
        res.redirect("/");
    });

    // define protected routes
    app.get("/adminStage.html", isLoggedIn, function(req, res) {
        res.sendFile(__dirname + '/client/html' + '/adminStage.html'); // refactor static directory to be global!
    });

    app.get("/stage.html", function(req, res) {
        if (req.isAuthenticated()) {
            res.sendFile(__dirname + '/client/html' + '/adminStage.html'); // refactor static directory to be global!
        } else {
            res.sendFile(__dirname + '/client/html' + '/stage.html'); // refactor static directory to be global!
        }
    });

    return {
        'isLoggedIn': isLoggedIn
    };
}

module.exports = initPassport;

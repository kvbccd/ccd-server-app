insert into tournament (id, tournament_mode) values (2, 'Fußball');

-- alle Gruppenphasen
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (21, 'Gruppe A', 2, 'gruppe', 1);
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (22, 'Gruppe B', 2, 'gruppe', 1);
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (23, 'Gruppe C', 2, 'gruppe', 1);
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (24, 'Gruppe D', 2, 'gruppe', 1);
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (25, 'Platzierungsrunde 1 bis 8', 2, 'ko', 2);
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (26, 'Platzierungsrunde 9 bis 12', 2, 'gruppe', 2);
insert into tournament_phase (id, name, tournament_id, type, phase_id) values (27, 'Platzierungsrunde 13 bis 17', 2, 'gruppe', 2);

-- alle Mannschaften
insert into team (id, name, name_short, tournament_id, type) values (101, 'KV Westfalen-Lippe 2', 'KVWL 2', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (102, 'KV Sachsen Dresden', 'KVS', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (103, 'KBV Kassenärztlicher Ballsportverein', 'KBV', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (104, 'KV Niedersachsen Nordlicht', 'KVN N', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (105, 'KV Brandenburg', 'KVBB', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (106, 'KV Niedersachsen Braunschweig', 'KVN BS', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (107, 'KV Westfalen-Lippe 1', 'KVWL 1', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (108, 'KV Schleswig-Holstein 2', 'KVSH 2', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (109, 'KV Baden-Württemberg', 'KVBW', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (110, 'KV Bayerns 2', 'KVB 2', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (111, 'KV Saarland', 'KV Saar', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (112, 'KV Thüringen', 'KVT', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (113, 'KV Hessen 1', 'KVH 1', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (114, 'KV Bayerns 1', 'KVB 1', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (115, 'KV Schleswig-Holstein 1', 'KVSH 1', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (116, 'KV Rheinland-Pfalz', 'KVRLP', 2, "real");
insert into team (id, name, name_short, tournament_id, type) values (117, 'KV Hessen 2', 'KVH 2', 2, "real");

insert into team (id, name, tournament_id, type) values (118, 'A1', 2, "virtual");
insert into team (id, name, tournament_id, type) values (119, 'A2', 2, "virtual");
insert into team (id, name, tournament_id, type) values (120, 'A3', 2, "virtual");
insert into team (id, name, tournament_id, type) values (121, 'A4', 2, "virtual");
insert into team (id, name, tournament_id, type) values (122, 'A5', 2, "virtual");
insert into team (id, name, tournament_id, type) values (123, 'B1', 2, "virtual");
insert into team (id, name, tournament_id, type) values (124, 'B2', 2, "virtual");
insert into team (id, name, tournament_id, type) values (125, 'B3', 2, "virtual");
insert into team (id, name, tournament_id, type) values (126, 'B4', 2, "virtual");
insert into team (id, name, tournament_id, type) values (127, 'C1', 2, "virtual");
insert into team (id, name, tournament_id, type) values (128, 'C2', 2, "virtual");
insert into team (id, name, tournament_id, type) values (129, 'C3', 2, "virtual");
insert into team (id, name, tournament_id, type) values (130, 'C4', 2, "virtual");
insert into team (id, name, tournament_id, type) values (131, 'D1', 2, "virtual");
insert into team (id, name, tournament_id, type) values (132, 'D2', 2, "virtual");
insert into team (id, name, tournament_id, type) values (133, 'D3', 2, "virtual");
insert into team (id, name, tournament_id, type) values (134, 'D4', 2, "virtual");
-- TODO
insert into team (id, name, tournament_id, type) values (135, 'Sieger 129', 2, "virtual");
insert into team (id, name, tournament_id, type) values (136, 'Verlierer 129', 2, "virtual");
insert into team (id, name, tournament_id, type) values (137, 'Sieger 130', 2, "virtual");
insert into team (id, name, tournament_id, type) values (138, 'Verlierer 130', 2, "virtual");
insert into team (id, name, tournament_id, type) values (139, 'Sieger 131', 2, "virtual");
insert into team (id, name, tournament_id, type) values (140, 'Verlierer 131', 2, "virtual");
insert into team (id, name, tournament_id, type) values (141, 'Sieger 132', 2, "virtual");
insert into team (id, name, tournament_id, type) values (142, 'Verlierer 132', 2, "virtual");

insert into team (id, name, tournament_id, type) values (143, 'Sieger 133', 2, "virtual");
insert into team (id, name, tournament_id, type) values (144, 'Verlierer 133', 2, "virtual");
insert into team (id, name, tournament_id, type) values (145, 'Sieger 134', 2, "virtual");
insert into team (id, name, tournament_id, type) values (146, 'Verlierer 134', 2, "virtual");
insert into team (id, name, tournament_id, type) values (147, 'Sieger 135', 2, "virtual");
insert into team (id, name, tournament_id, type) values (148, 'Verlierer 135', 2, "virtual");
insert into team (id, name, tournament_id, type) values (149, 'Sieger 136', 2, "virtual");
insert into team (id, name, tournament_id, type) values (150, 'Verlierer 136', 2, "virtual");

--alle initialen Platzierungen
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (21, 101, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (21, 102, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (21, 103, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (21, 104, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (21, 105, 0, 0, 0, 0, 0);

insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (22, 106, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (22, 107, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (22, 108, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (22, 109, 0, 0, 0, 0, 0);

insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (23, 110, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (23, 111, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (23, 112, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (23, 113, 0, 0, 0, 0, 0);

insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (24, 114, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (24, 115, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (24, 116, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (24, 117, 0, 0, 0, 0, 0);

insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (26, 120, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (26, 125, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (26, 129, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (26, 133, 0, 0, 0, 0, 0);

insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (27, 121, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (27, 122, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (27, 126, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (27, 130, 0, 0, 0, 0, 0);
insert into tournament_phase_result (tournament_phase_id, team_id, games_played, points, balls_scored, balls_conceded, rank) values (27, 134, 0, 0, 0, 0, 0);

--alle Spiele
--Gruppe A
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(101, 21, 101, 105, '09:00', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(102, 21, 102, 103, '09:00', 'Feld 2');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(109, 21, 104, 101, '09:40', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(110, 21, 103, 105, '09:40', 'Feld 2');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(117, 21, 104, 102, '10:20', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(118, 21, 103, 101, '10:20', 'Feld 2');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(125, 21, 105, 104, '11:00', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(126, 21, 101, 102, '11:00', 'Feld 2');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(127, 21, 104, 103, '11:30', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(128, 21, 102, 105, '11:30', 'Feld 2');
--Gruppe B
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(103, 22, 107, 109, '09:00', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(104, 22, 106, 108, '09:00', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(111, 22, 107, 106, '09:40', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(112, 22, 108, 109, '09:40', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(119, 22, 109, 106, '10:20', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(120, 22, 108, 107, '10:20', 'Feld 4');
--Gruppe C
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(105, 23, 111, 113, '09:20', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(106, 23, 110, 112, '09:20', 'Feld 2');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(113, 23, 111, 110, '10:00', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(114, 23, 112, 113, '10:00', 'Feld 2');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(121, 23, 113, 110, '10:40', 'Feld 1');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(122, 23, 112, 111, '10:40', 'Feld 2');
--Gruppe D
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(107, 24, 115, 117, '09:20', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(108, 24, 114, 116, '09:20', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(115, 24, 115, 114, '10:00', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(116, 24, 116, 117, '10:00', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(123, 24, 117, 114, '10:40', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(124, 24, 116, 115, '10:40', 'Feld 4');
--1 bis 8
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos) values(129, 25, 118, 132, '13:00', 'Feld 1', 135, 133, 'A');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos) values(130, 25, 123, 128, '13:00', 'Feld 2', 136, 134, 'A');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos) values(131, 25, 127, 124, '13:30', 'Feld 1', 135, 133, 'B');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos) values(132, 25, 131, 119, '13:30', 'Feld 2', 136, 134, 'B');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos, label) values(133, 25, 136, 140, '14:05', 'Feld 1', 138, 137, 'A', 'kl. HF');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos) values(134, 25, 138, 142, '14:05', 'Feld 2', 138, 137, 'B');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos, label) values(135, 25, 135, 139, '14:30', 'Feld 1', 140, 139, 'A', 'HF');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, winner_match_id, loser_match_id, match_pos) values(136, 25, 137, 141, '14:30', 'Feld 2', 140, 139, 'B');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, label) values(137, 25, 144, 146, '15:05', 'Feld 1', 'Platz 7');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, label) values(138, 25, 143, 145, '15:05', 'Feld 2', 'Platz 5');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, label) values(139, 25, 148, 150, '15:30', 'Feld 1', 'Platz 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field, label) values(140, 25, 147, 149, '16:05', 'Feld 1', 'Finale');
--9 bis 12
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(141, 26, 125, 133, '13:20', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(142, 26, 120, 129, '13:20', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(143, 26, 125, 120, '14:40', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(144, 26, 129, 133, '14:40', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(145, 26, 133, 120, '15:20', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(146, 26, 129, 125, '15:20', 'Feld 4');
--13 bis 17
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(147, 27, 121, 134, '13:00', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(148, 27, 122, 126, '13:00', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(149, 27, 130, 121, '13:40', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(150, 27, 126, 134, '13:40', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(151, 27, 130, 122, '14:20', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(152, 27, 126, 121, '14:20', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(153, 27, 134, 130, '15:00', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(154, 27, 121, 122, '15:00', 'Feld 4');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(155, 27, 130, 126, '15:40', 'Feld 3');
insert into match (id, tournament_phase_id, teamA, teamB, match_time, field) values(156, 27, 122, 134, '15:40', 'Feld 4');

insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 1, 129, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 2, 132, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 3, 142, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 3, 143, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 3, 145, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 4, 147, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 4, 149, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 4, 152, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 4, 154, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 5, 148, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 5, 151, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 5, 154, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (21, 5, 156, 'A');

insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 1, 130, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 2, 131, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 3, 141, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 3, 143, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 3, 146, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 4, 148, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 4, 150, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 4, 152, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (22, 4, 155, 'B');

insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 1, 131, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 2, 130, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 3, 142, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 3, 144, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 3, 146, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 4, 149, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 4, 151, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 4, 153, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (23, 4, 155, 'A');

insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 1, 132, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 2, 129, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 3, 141, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 3, 144, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 3, 145, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 4, 147, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 4, 150, 'B');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 4, 153, 'A');
insert into tournament_phase_conclusion (tournament_phase_id, position, next_match_id, match_pos) values (24, 4, 156, 'B');

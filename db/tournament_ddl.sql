drop table tournament_phase_conclusion;
drop table tournament_phase_result;
drop table tournament_result;
drop table team_result;
drop table result;
drop table match;
drop table team;
drop table tournament_phase;
drop table phase;
drop table tournament;

CREATE TABLE tournament (
	id integer PRIMARY KEY,
	tournament_mode text,
	start_date text,
	end_date text,
	location text
);

CREATE TABLE phase (
	id integer PRIMARY KEY,
	type text
);

CREATE TABLE tournament_phase (
	id integer PRIMARY KEY,
	name text,
	tournament_id text,
	type text,
 	phase_id integer,
 	FOREIGN KEY (tournament_id) REFERENCES tournament (id),
 	FOREIGN KEY (phase_id) REFERENCES phase (id),
 	CHECK (type = "gruppe" or type = "ko")
 );

CREATE TABLE tournament_phase_result (
	tournament_phase_id integer,
	team_id integer,
	games_played integer,
	points integer,
	balls_scored integer,
	balls_conceded integer,
	rank integer,
 	FOREIGN KEY (tournament_phase_id) REFERENCES tournament_phase (id),
 	FOREIGN KEY (team_id) REFERENCES team (id)
 );

CREATE TABLE match (
	id integer PRIMARY KEY,
	tournament_phase_id integer,
	teamA integer,
	teamB integer,
	match_time text,
	field text,
	winner_match_id integer,
	loser_match_id integer,
	match_pos text,
	label text,
	FOREIGN KEY (tournament_phase_id) REFERENCES tournament_phase (id),
	FOREIGN KEY (teamA) REFERENCES team (id),
	FOREIGN KEY (teamB) REFERENCES team (id)
);

CREATE TABLE result (
	match_id integer,
	rank integer,
	points_team_A integer,
	points_team_B integer,
	CONSTRAINT result_pk PRIMARY KEY (match_id, rank),
	FOREIGN KEY (match_id) REFERENCES match (id)
);

CREATE TABLE team (
	id integer PRIMARY KEY,
	name text,
	name_short text,
	tournament_id integer,
	type text,
	FOREIGN KEY (tournament_id) REFERENCES tournament (id),
	CHECK (type = "real" or type = "virtual")
);

CREATE TABLE team_result (
	id integer,
	team_id integer,
	CONSTRAINT team_result_pk PRIMARY KEY (id, team_id),
	FOREIGN KEY (team_id) REFERENCES team (id)
);

CREATE TABLE tournament_phase_conclusion (
	tournament_phase_id integer,
	position integer,
	next_match_id integer,
	match_pos text,
	FOREIGN KEY (tournament_phase_id) REFERENCES tournament_phase (id),
	FOREIGN KEY (next_match_id) REFERENCES match (id)
);

CREATE TABLE tournament_result (
    team_id integer,
    tournament_id integer,
    position integer,
    FOREIGN KEY (team_id) REFERENCES team (id),
    FOREIGN KEY (tournament_id) REFERENCES tournament (id)
);

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);

/*
* Test the /GET turnier route
*/
describe('/GET /turnier', () => {
    it('it should return 200', (done) => {
      chai.request(server)
          .get('/turnier')
          .end((err, res) => {
              res.should.have.status(200);
              // res.body.should.be.a('array');
              // res.body.length.should.be.eql(0);
            done();
          });
    });
});

/*
* Test the /GET turnier/test route
*/
describe('/GET /turnier/test/test', () => {
    it('it should return 200', (done) => {
      chai.request(server)
          .get('/turnier/test/test')
          .end((err, res) => {
              res.should.have.status(200);
              // res.body.should.be.a('array');
              // res.body.length.should.be.eql(0);
            done();
          });
    });
});
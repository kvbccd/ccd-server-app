var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);
/*
* Test the /GET / route
*/
describe('/GET /', () => {
    it('it should return 404', (done) => {
      chai.request(server)
          .get('/')
          .end((err, res) => {
              res.should.have.status(404);
            done();
          });
    });
});
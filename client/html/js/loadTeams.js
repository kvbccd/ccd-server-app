$(document).ready(function() {
    var tournamentId = parseInt(localStorage.getItem("tournamentId"));
    var tournamentType = localStorage.getItem("tournamentType");
    if (tournamentId !== 1 && tournamentId !== 2) {
        return;
    }
    $("#tournament-type").append(tournamentType);

    var URL = "teams/" + tournamentId;

    $.getJSON(URL, function(teams) {
        $.each(teams, function(i, team) {
            var htmlStr = '<a href="stage.html" class="list-group-item text-center" onclick="javascript:localStorage.setItem(\'teamName\', \'' + team.name
                + '\', 1); localStorage.setItem(\'teamId\', \'' + team.id
                + '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + team.name + '</a>';
            $("div#team-list").append(htmlStr);
        });
    });
});
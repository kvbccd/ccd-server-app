$(document).ready(function() {
    var URL = "/turnier";

    $.getJSON(URL, function(tournaments) {
        $.each(tournaments, function(ndx, tournament) {
            $("#accordion").append(
                '<div class="panel panel-default">' +
                    '<div class="panel-heading">' +
                        '<h4 class="panel-title text-center">' +
                            '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'
                                + tournament.id + '">' + tournament.type + ' ' + tournament.start_date + ' ' + tournament.location +
                        '</h4>' +
                    '</div>' +
                    '<div id="collapse' + tournament.id + '" class="panel-collapse collapse">' +
                        '<div class="list-group">' +
                            '<a href="stages.html" class="list-group-item text-center" onclick="javascript:localStorage.setItem(\'tournamentId\', ' + tournament.id
                                + ', 1); localStorage.removeItem(\'teamId\'); localStorage.setItem(\'tournamentType\', \'' + tournament.type
                                + '\', 1); return true;">Turnierphasen</a>' +
                            '<a href="overview.html" class="list-group-item text-center" onclick="javascript:localStorage.setItem(\'tournamentId\', ' + tournament.id
                                + ', 1); localStorage.removeItem(\'teamId\'); localStorage.setItem(\'tournamentType\', \'' + tournament.type
                                + '\', 1); return true;">Spielübersicht</a>' +
                            '<a href="teams.html" class="list-group-item text-center" onclick="javascript:localStorage.setItem(\'tournamentId\', ' + tournament.id
                                + ', 1); localStorage.removeItem(\'teamId\'); localStorage.setItem(\'tournamentType\', \'' + tournament.type
                                + '\', 1); return true;">Mannschaften</a>' +
                            '<a href="results.html" class="list-group-item text-center" onclick="javascript:localStorage.setItem(\'tournamentId\', ' + tournament.id
                                + ', 1); localStorage.removeItem(\'teamId\'); localStorage.setItem(\'tournamentType\', \'' + tournament.type
                                + '\', 1); return true;">Ergebnis</a>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
        });
    });
});

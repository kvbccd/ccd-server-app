$(document).ready(function() {
    var tournamentId = parseInt(localStorage.getItem("tournamentId"));
    var tournamentType = localStorage.getItem("tournamentType");
    if (tournamentId !== 1 && tournamentId !== 2) {
        return;
    }
    $("#tournament-type").append(tournamentType);

    var URL = "finalresults/" + tournamentId;
    //var URL = "../dummydaten/results.json";

    $.getJSON(URL, function(teams) {
    	if(teams.length > 0) {
            $.each(teams, function(i, team) {
                var htmlStr = '<a href="stage.html" class="list-group-item text-left" onclick="javascript:localStorage.setItem(\'teamName\', \'' + team.name + '\', 1); localStorage.setItem(\'teamId\', \'' + team.id + '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + team.position + ". " + team.name + '</a>';
                $("div#team-list").append(htmlStr);
            });
    	} else {
    		$("div#team-list").append("Das endgültige Ergebnis des Turniers steht noch nicht fest.");
    	}
    });
});
$(document).ready(function() {
    var tournamentId = parseInt(localStorage.getItem("tournamentId"));
    var tournamentType = localStorage.getItem("tournamentType");
    if (tournamentId !== 1 && tournamentId !== 2) {
        return;
    }
    $("#tournament-type").append(tournamentType);

    var URL = "turnier/" + tournamentId;

    $.getJSON(URL, function(stages) {
        $.each(stages, function(i, item) {
            var separator = '<p></p>';
            var htmlStr = '<a href="adminStage.html" class="list-group-item text-center" onclick="javascript:localStorage.setItem(\'tournamentPhaseName\', \'' + item.name
                + '\', 1); localStorage.removeItem(\'teamId\'); localStorage.setItem(\'tournamentPhaseId\', \'' + item.id
                + '\', 1); localStorage.setItem(\'phaseId\', \'' + item.phase_id
                + '\', 1); localStorage.setItem(\'phaseType\', \'' + item.type
                + '\', 1); localStorage.setItem(\'clickedOnTeam\', 0, 1); return true;">' + item.name + '</a>';
            if (i === 3 || i === 4) {
                htmlStr = htmlStr + separator;
            }
            $("div#stage-list").append(htmlStr);
        });
    });
});

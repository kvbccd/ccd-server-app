$(document).ready(function() {

    var tournamentId = parseInt(localStorage.getItem("tournamentId"));
    var tournamentType = localStorage.getItem("tournamentType");
    var tournamentPhaseId = parseInt(localStorage.getItem("tournamentPhaseId")); // unused
    var tournamentPhaseName = localStorage.getItem("tournamentPhaseName");
    var teamName = localStorage.getItem("teamName");
    var teamId = parseInt(localStorage.getItem("teamId"));
    var phaseId = parseInt(localStorage.getItem("phaseId"));
    var clickedOnTeam = parseInt(localStorage.getItem("clickedOnTeam")) === 1 ? true : false;
    var phaseType = localStorage.getItem("phaseType");

    if (tournamentId !== 1 && tournamentId !== 2) {
        return;
    }

    $("#tournament-type").append(tournamentType);

    // websocket client
    const url = "ws://localhost:3000/echo";
    const ws = new WebSocket(url);

    ws.onopen = function(e) {
        log("web socet opened");
        var data = {
            "type": "",
            "content": {
            	"tournamentId": tournamentId,
                "tournamentPhaseId": tournamentPhaseId,
                "teamId": teamId
            }
        };
        var message = JSON.stringify(data);
        ws.send(message);
    };

    ws.onclose = function(e) {
        log("web socket closed");
    };
    ws.onerror = function(e) {
        log("web socket error");
    };

    function log(s) {
        console.log('Client log: ' + s);
    }

    ws.onmessage = function(e) {
        var response = JSON.parse(e.data.toString());
        if (response.type === 'allMatches') {
            $.each(response.content, function(indx, match) {
                var resultStr = '';
                $.each(match.results, function(j, result) {
                    resultStr += result.points_team_A + ':' + result.points_team_B +
                        '<br>'
                });
                if (resultStr.length === 0) {
                    resultStr = '-:-';
                }

                var htmlStrGroup =
                    '<a href="stage.html" onclick="javascript:localStorage.setItem(\'tournamentPhaseId\', \'' + match.phase_id +
                    '\', 1); localStorage.setItem(\'tournamentPhaseName\', \'' + match.phase_name +
                    '\', 1); localStorage.removeItem(\'teamId\'); localStorage.removeItem(\'teamName\'); localStorage.removeItem(\'clickedOnTeam\'); return true;">'
                    + match.phase_name_short + '</a>';

                var htmlStrTeamA =
                    '<a href="stage.html" onclick="javascript:localStorage.setItem(\'teamName\', \'' + match.team_A +
                    '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'teamId\', \'' + match.team_A_id +
                    '\', 1); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + match.team_A + '</a>';

                var htmlStrTeamB =
                    '<a href="stage.html" onclick="javascript:localStorage.setItem(\'teamName\', \'' + match.team_B +
                    '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'teamId\', \'' + match.team_B_id +
                    '\', 1); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + match.team_B + '</a>';

                var matchHtml = '<tr>' +
                    '<td>' + match.match_id + '</td>' +
                    '<td>' + htmlStrGroup + '</td>' +
                    '<td>' + htmlStrTeamA + '</td>' +
                    '<td>' + htmlStrTeamB + '</td>' +
                    '<td> ' + resultStr + ' </td>' +
                    '<td>' + match.match_time + '</td>' +
                    '<td>' + match.field + '</td>' +
                    '</tr>';

                if (indx === 0) {
                    $("tbody#match-list").empty();
                }
                if (match.label) {
                	$("tbody#match-list").append('<tr><td><b>' + match.label + '</b></td><td></td><td></td><td></td><td></td><td></td></tr>');
                }
                $("tbody#match-list").append(matchHtml);
            });
        }

        if (response.updateTime) {
            $('span#update-time').animate({'opacity': 0}, 1000, function () {
                $(this).text('Letzte Aktualisierung: ' + response.updateTime).animate({'opacity': 1}, 1000);
            });
        }
    };
});

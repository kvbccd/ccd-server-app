$(document).ready(function() {

    var tournamentId = parseInt(localStorage.getItem("tournamentId"));
    var tournamentType = localStorage.getItem("tournamentType");
    var tournamentPhaseId = parseInt(localStorage.getItem("tournamentPhaseId")); // unused
    var tournamentPhaseName = localStorage.getItem("tournamentPhaseName");
    var teamName = localStorage.getItem("teamName");
    var teamId = parseInt(localStorage.getItem("teamId"));
    var phaseId = parseInt(localStorage.getItem("phaseId"));
    var clickedOnTeam = parseInt(localStorage.getItem("clickedOnTeam")) === 1 ? true : false;
    var phaseType = localStorage.getItem("phaseType");

    var btnIdPrefix = "setRankButton-";
    var btnMatchPrefix = "setMatchButton-";
    var inputIdPrexix = "rank-";

    if (tournamentId !== 1 && tournamentId !== 2) {
        return;
    }

    //Beim Fußall keine Eingabe des Satzes. Dementsprechend wird das Dropdown nicht angezeigt
    if (String(tournamentId) === String(2)) {
    	$("#rank").hide();
    	$("#labelRank").hide();
    }

    // Falls navigation durch Klick auf Mannschaft dann zeige nicht
    if (clickedOnTeam) {
        $("#resultForm").hide();
        $("#calcTableButton").hide();
        $("#concludePhaseButton").hide();
    }

    $("#tournament-type").append(tournamentType);
    $("#tournament-phase-name").append(clickedOnTeam ? "Team Überblick " + teamName : tournamentPhaseName);

    var loadMatches = function() {
    	//Tabelle wird geleert und neu geladen
    	$("tbody#match-list").children().remove();
    	$.getJSON(matchesURL, showMatchTable);
    };

    var loadTable = function() {
   		//Tabelle wird geleert und neu geladen
   		$("tbody#group-table").children().remove();
   		$.getJSON(groupTableURL, showGroupTable);
    };

    var fillNextMatch = function() {
    	var matchId = String(this.id).replace(btnMatchPrefix, '');
    	var data = {
    		action : 'fillNextMatch',
    		matchId : matchId
    	};
    	$.post("phasetable/", data, setTimeout(loadMatches, 500));
    };

    //Zeigt die Ergebnisse der Spiele
    var showMatchTable = function(response) {
    	var matchSelectOptionHtml = "<option/>";
        $.each(response, function(i, match) {
            var resultStr = '';
            $.each(match.results, function(j, result) {
                resultStr += result.points_team_A + ':' + result.points_team_B +
                    '<br>'
            });
            if (resultStr.length === 0) {
                resultStr = '-:-';
            }

            var htmlStrTeamA = match.team_A;
            var htmlStrTeamB = match.team_B;

            var htmlStrFollowerButton = '';
            if (String(match.match_follower) === String(1)) {
            	var btnId = btnMatchPrefix + match.match_id;
            	htmlStrFollowerButton = '<td><button id="' + btnId + '" type="button" class="btn">OK</button></td>';
            };
            var matchHtml = '<tr>' +
                '<td>' + match.match_id + '</td>' +
                '<td>' + htmlStrTeamA + '</td>' +
                '<td>' + htmlStrTeamB + '</td>' +
                '<td> ' + resultStr + ' </td>' +
                '<td>' + match.match_time + '</td>' +
                '<td>' + match.field + '</td>' +
                htmlStrFollowerButton +
                '</tr>';

            if (match.label) {
              	$("tbody#match-list").append('<tr><td><b>' + match.label + '</b></td><td></td><td></td><td></td><td></td><td></td></tr>');
            }
            $("tbody#match-list").append(matchHtml);
            matchSelectOptionHtml += "<option>" + match.match_id + "</option>";
            $('#' + btnMatchPrefix + match.match_id).click(fillNextMatch);
        });
        $("#matchSelect").html(matchSelectOptionHtml);
        $("#matchSelect").selectpicker('refresh');
    };

    //schickt den Wert für "Los" an den Server
    var setRank = function() {
    	var rowTeamId = String(this.id).replace(btnIdPrefix, '');
    	var inputVal = $('#' + inputIdPrexix + rowTeamId).val();
    	var data = {
    		action : 'setRank',
    		teamId : rowTeamId,
    		rank : inputVal
    	};
    	$.post("phasetable/", data, setTimeout(loadTable, 500));
    };

    //Zeigt die Tabelle der Gruppe an
    var showGroupTable = function(rows) {
    	$.each(rows, function(i, row) {
    		var htmlStr = row.team_name;
    		var btnId = btnIdPrefix + row.team_id;

            var inputSetRank = !clickedOnTeam ? '<input name="inputRank" type="number" min="0" class"form-control" value="' + row.rank + '" id="' + inputIdPrexix + row.team_id + '">' : row.rank;
            var colSetRankButton = !clickedOnTeam ? '<td> <button id="' + btnId + '" type="button" class="btn">Set</button> </td>' : '';

    		var rowHtml =
                '<tr>' +
        			'<td>' + row.position + '.</td>' +
        			'<td>' + htmlStr + '</td>' +
        			'<td>' + row.games_played + '</td>' +
        			'<td>' + row.points + '</td>' +
        			'<td>' + row.balls_scored + ':' + row.balls_conceded + '</td>' +
        			'<td width="20">' + inputSetRank + '</td>' +
                    colSetRankButton +
                '</tr>';

             $("tbody#group-table").append(rowHtml);
             $('#' + btnId).click(setRank);
    	});
    };

    var matchesURL = "phase/" + tournamentPhaseId;
    var groupTableURL = "phasetable/" + tournamentPhaseId;

    $.getJSON(matchesURL, showMatchTable);

    if (phaseType === 'gruppe' && !clickedOnTeam) {

        // Tabelle wird gebaut nur für die Gruppenphase
        var htmlGroupTable = '<h3>Tabelle</h3> <table class="table"> <thead> <tr> <th>Pos.</th> <th>Mannschaft</th> <th>Sp.</th> <th>Pkte.</th> <th>Tore</th> <th>Los</th> <th></th></tr> </thead> <!-- Tabelle eingefügen --> <tbody id="group-table"> </tbody> </table>';
        $("div#table").append(htmlGroupTable);

        $.getJSON(groupTableURL, showGroupTable);
    }

    //Zeige den Button zur Berechnung der Tabelle nur in der Gruppenphase
    if (phaseType === 'gruppe') {
		$("#calcTableButton").click(function() {
			var data = {
    	  		action : 'calculateTable',
    	  		tournamentId: tournamentId,
    	  		tournamentPhaseId: tournamentPhaseId
    	  	};
    	  	$.post("phasetable/", data, setTimeout(loadTable, 500));
    	});
    } else {
    	$("#calcTableButton").hide();
    }

    if (phaseId === 1) {
    	$("#concludePhaseButton").click(function(){
        	var data = {
        		action : 'concludePhase',
        		phaseId : phaseId,
        		tournamentPhaseId : tournamentPhaseId
        	};
        	$.post("phasetable/", data);
    	});
    } else {
    	$("#concludePhaseButton").hide();
    }

    $("#enterMatchResultButton").click(function() {
    	var pointsA = parseInt($('#pointsA').val());
    	var pointsB = parseInt($('#pointsB').val());
    	var matchId = parseInt($('#matchSelect').val());
    	var rank = parseInt($('#rank').val());
    	if (isNaN(pointsA) || isNaN(pointsB) || isNaN(matchId) || isNaN(rank)) {
    		alert("Die Eingaben sind nicht gültig");
    		return;
    	}
    	var data = {
    		action : 'enterMatchResult',
    		matchId : matchId,
    		rank : rank,
    		pointsA : pointsA,
    		pointsB : pointsB
    	};
    	$.post("phasetable/", data, setTimeout(loadMatches, 500));
    });
});

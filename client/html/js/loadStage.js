$(document).ready(function() {

    var tournamentId = parseInt(localStorage.getItem("tournamentId"));
    var tournamentType = localStorage.getItem("tournamentType");
    var tournamentPhaseId = parseInt(localStorage.getItem("tournamentPhaseId")); // unused
    var tournamentPhaseName = localStorage.getItem("tournamentPhaseName");
    var teamName = localStorage.getItem("teamName");
    var teamId = parseInt(localStorage.getItem("teamId"));
    var phaseId = parseInt(localStorage.getItem("phaseId"));
    var clickedOnTeam = parseInt(localStorage.getItem("clickedOnTeam")) === 1 ? true : false;
    var phaseType = localStorage.getItem("phaseType");

    if (tournamentId !== 1 && tournamentId !== 2) {
        return;
    }

    $("#tournament-type").append(tournamentType);
    $("#tournament-phase-name").append(clickedOnTeam ? "Team Überblick " + teamName : tournamentPhaseName);

    // websocket client
    const url = "ws://localhost:3000/echo";
    const ws = new WebSocket(url);

    ws.onopen = function(e) {
        log("web socet opened");
        var data = {
            "type": "",
            "content": {
            	"tournamentId": tournamentId,
                "tournamentPhaseId": tournamentPhaseId,
                "teamId": teamId
            }
        };
        var message = JSON.stringify(data);
        ws.send(message);
    };

    ws.onclose = function(e) {
        log("web socket closed");
    };
    ws.onerror = function(e) {
        log("web socket error");
    };

    function log(s) {
        console.log('Client log: ' + s);
    }

    ws.onmessage = function(e) {
        var response = JSON.parse(e.data.toString());
        if (response.type === 'match') {
            $.each(response.content, function(indx, match) {
                var resultStr = '';
                $.each(match.results, function(j, result) {
                    resultStr += result.points_team_A + ':' + result.points_team_B +
                        '<br>'
                });
                if (resultStr.length === 0) {
                    resultStr = '-:-';
                }

                var htmlStrTeamA = teamId === match.team_A_id && clickedOnTeam ? match.team_A :
                    '<a href="stage.html" onclick="javascript:localStorage.setItem(\'teamName\', \'' + match.team_A +
                    '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'teamId\', \'' + match.team_A_id +
                    '\', 1); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + match.team_A + '</a>';
                var htmlStrTeamB = teamId === match.team_B_id && clickedOnTeam ? match.team_B :
                    '<a href="stage.html" onclick="javascript:localStorage.setItem(\'teamName\', \'' + match.team_B +
                    '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'teamId\', \'' + match.team_B_id +
                    '\', 1); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + match.team_B + '</a>';

                var matchHtml = '<tr>' +
                    '<td>' + match.match_id + '</td>' +
                    '<td>' + htmlStrTeamA + '</td>' +
                    '<td>' + htmlStrTeamB + '</td>' +
                    '<td> ' + resultStr + ' </td>' +
                    '<td>' + match.match_time + '</td>' +
                    '<td>' + match.field + '</td>' +
                    '</tr>';

                if (indx === 0) {
                    $("tbody#match-list").empty();
                }
                if (match.label) {
                	$("tbody#match-list").append('<tr><td><b>' + match.label + '</b></td><td></td><td></td><td></td><td></td><td></td></tr>');
                }
                $("tbody#match-list").append(matchHtml);
            });
        }

        if (response.type === 'group_table' && !clickedOnTeam & phaseType === 'gruppe') {
            $("div#table").empty();

            // Tabelle wird gebaut nur für die Gruppenphase
            var htmlGroupTable = '<h3>Tabelle</h3> <table class="table"> <thead> <tr> <th>Pos.</th> <th>Mannschaft</th> <th>Sp.</th> <th>Pkte.</th> <th>Tore</th> </tr> </thead> <!-- Tabelle eingefügen --> <tbody id="group-table"> </tbody> </table>';
            $("div#table").append(htmlGroupTable);

            $.each(response.content, function(indx, row) {
                if (indx === 0) {
                    // Tabelle wird gebaut nur für die Gruppenphase
                    $("tbody#group-table").empty();
                }

                var htmlStr = teamId === row.team_id && clickedOnTeam === 1 ? row.team_name :
                    '<a href="stage.html" onclick="javascript:localStorage.setItem(\'teamName\', \'' + row.team_name +
                    '\', 1); localStorage.setItem(\'teamId\', \'' + row.team_id +
                    '\', 1); localStorage.removeItem(\'tournamentPhaseId\'); localStorage.setItem(\'clickedOnTeam\', 1, 1); return true;">' + row.team_name + '</a>'

                var rowHtml = '<tr>' +
                    '<td>' + row.position + '.</td>' +
                    '<td>' + htmlStr + '</td>' +
                    '<td>' + row.games_played + '</td>' +
                    '<td>' + row.points + '</td>' +
                    '<td>' + row.balls_scored + ':' + row.balls_conceded + '</td>' +
                    '</tr>';

                $("tbody#group-table").append(rowHtml);
            });
        }

        if (response.updateTime) {
            $('span#update-time').animate({'opacity': 0}, 1000, function () {
                $(this).text('Letzte Aktualisierung: ' + response.updateTime).animate({'opacity': 1}, 1000);
            });
        }
    };
});

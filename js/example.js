

// Erste Seite

{
    tournaments: [{
        tournament_mode: "Volleyball",
        start_date: "2017-05-17",
        end_date: "2017-05-17",
        location: "München",
        teams: [{
            id: 1,
            name: "KVB"
        }, {
            id: 2,
            name: "KVH"
        }]
    }, {
        tournament_mode: "Fußball",
        start_date: "2017-05-17",
        end_date: "2017-05-17",
        location: "München",
        teams: [{
            id: 3,
            name: "KVB"
        }, {
            id: 4,
            name: "KVH"
        }]
    }]
}


// Zweite Seite

{
    tournaments: [{
        tournament_mode: "Volleyball",
        tournament_phases: [{
            id: 1,
            identifier: "Gruppe A",
            phase_id: 1
        }, {
            id: 2,
            identifier: "Gruppe B",
            phase_id: 1
        }, {
            id: 3,
            identifier: "Gruppe C",
            phase_id: 1
        }, {
            id: 4,
            identifier: "Gruppe D",
            phase_id: 1
        }, {
            id: 5,
            identifier: "Viertelfinale",
            phase_id: 2
        }, {
            id: 6,
            identifier: "PR1bis4",
            phase_id: 3
        }, {
            id: 7,
            identifier: "PR5bis8",
            phase_id: 3
        }, {
            id: 8,
            identifier: "PR9bis12",
            phase_id: 3
        }, {
            id: 9,
            identifier: "PR13bis16",
            phase_id: 3
        }, {
            id: 10,
            identifier: "PR17bis20",
            phase_id: 3
        }, {
            id: 11,
            identifier: "PR21bis24",
            phase_id: 3
        }]
    }, {
        tournament_mode: "Fußball",
        tournament_phases: [{
            id: 12,
            identifier: "Gruppe A",
            phase_id: 1
        }, {
            id: 13,
            identifier: "Gruppe B",
            phase_id: 1
        }]
    }]
}


// Dritte Seite TODO

{
    tournament: {
        tournament_mode: "Volleyball",
        teams: [{
            id: 1,
            name: "KVB"
        }, {
            id: 2,
            name: "KVH"
        }
    }
}



var server = require('../server');
var dateFormat = require('dateformat');

var WebSocket = require('ws');
var WebSocketServer = WebSocket.Server;
var wss = new WebSocketServer({
    server
});

const OPEN = WebSocket.OPEN;
var fs = require("fs");

var dbFunctions = require('./dbFunctions');

var clientCount = 0;

// server socket connection
wss.on('connection', function connection(ws) {
    console.log('connection opened');
    ws.on('message', function incoming(data) {
        var inputData = JSON.parse(data);
        var tournamentPhaseId = inputData.content.tournamentPhaseId;
        var tournamentId = inputData.content.tournamentId;
        var teamId = inputData.content.teamId;

		clientCount = 0;
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                clientCount++;
            }
        });
        var now = new Date();
        console.log(`${now} clients connected: ${clientCount}`);

        dbFunctions.retrieveMatchesFromDB(parseInt(tournamentPhaseId), parseInt(teamId), function(resultObj) {
            var outputData = JSON.stringify({
                "type": 'match',
                "content": resultObj
            });
            ws.send(outputData);
        });

        dbFunctions.retrieveTableFromDB(parseInt(tournamentPhaseId), function(resultObj) {
            var outputData = JSON.stringify({
                "type": 'group_table',
                "content": resultObj
            });
            ws.send(outputData);
        });

        dbFunctions.retrieveAllMatchesFromDB(parseInt(tournamentId), function(resultObj) {
        	var outputData = JSON.stringify({
        		"type" : 'allMatches',
        		"content" : resultObj
        	});
        	ws.send(outputData);
        });

        fs.watch(global.config.db.sqlite.url, function listener(eventType, filename) {
            if (eventType === 'rename' || eventType === 'change') {
                var now = new Date();
                dbFunctions.retrieveMatchesFromDB(parseInt(tournamentPhaseId), parseInt(teamId), function(resultObj) {
                    var outputData = JSON.stringify({
                        "type": 'match',
                        "updateTime": dateFormat(now, "dd.mm.yyyy HH:MM:ss"),
                        "content": resultObj
                    });
                    if (ws.readyState === OPEN) {
                        ws.send(outputData);
                    }
                });

                dbFunctions.retrieveTableFromDB(parseInt(tournamentPhaseId), function(resultObj) {
                    var outputData = JSON.stringify({
                        "type": 'group_table',
                        "updateTime": dateFormat(now, "dd-mm-yyyy HH:MM:ss"),
                        "content": resultObj
                    });
                    if (ws.readyState === OPEN) {
                        ws.send(outputData);
                    }
                });

                dbFunctions.retrieveAllMatchesFromDB(parseInt(tournamentId), function(resultObj) {
                    var outputData = JSON.stringify({
                        "type": 'allMatches',
                        "updateTime": dateFormat(now, "dd.mm.yyyy HH:MM:ss"),
                        "content": resultObj
                    });
                    if (ws.readyState === OPEN) {
                        ws.send(outputData);
                    }
                });

            }
        });
    });

    ws.on('open', function open() {
        console.log('connection opened'); // never reached
    });

    ws.on('close', function close() {
        console.log('connection closed');
        clientCount--;
        var now = new Date();
        console.log(`${now} clients connected: ${clientCount}`);
    });

});

module.exports = wss;

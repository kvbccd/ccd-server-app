module.exports = genericModel;

function genericModel(model) {
	return {
		find: function(params, cb) { // get all
			try {
				var results = model.find(params, cb);
				//cb(null, results);
			} catch(error) {
				cb(error, null);
			}
		},
		findById: function(id, cb) { // get id
			this.find({
				id: id
			}, cb);
		},
		findByIdAndUpdate: function(id, data, cb) { // put
			try {
				var results = model.update(data);
				cb(null, results);
			} catch(error) {
				cb(error, null);
			}
		},
		save: function(data, cb) { // post
			try {
				var results = model.save(data);
				cb(null, results);
			} catch(error) {
				cb(error, null);
			}
		}
	}
}

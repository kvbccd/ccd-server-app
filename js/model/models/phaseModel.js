module.exports = phaseModel;

function phaseModel() {

    var retrieveMatchesFromDB = function(id, callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];

        db.serialize(function() {
            var selectStmt = "select m.id as match_id, m.teamA as team_A_id, t1.name as team_A, m.teamB as team_B_id, t2.name as team_B," +
                " m.match_time, m.field, r.rank, r.points_team_A, r.points_team_B," +
                " case when (m.winner_match_id is not null) then 1 else 0 end as match_follower, m.label from match m" +
                " join team t1 on m.teamA = t1.ID" +
                " join team t2 on m.teamB = t2.ID" +
                " left join result r on m.id = r.match_id" +
                " where m.tournament_phase_id = $tournamentPhaseId";
            db.each(selectStmt, {
                    $tournamentPhaseId: id
                },
                function(err, row) {
                    if (typeof row !== "undefined") {
                        var lastMatchId = queryResult.length > 0 ? queryResult[queryResult.length - 1].match_id : 0;
                        if (row.match_id !== lastMatchId) {
                            // open new match entry...
                            queryResult.push({
                                match_id: row.match_id,
                                team_A_id: row.team_A_id,
                                team_A: row.team_A,
                                team_B_id: row.team_B_id,
                                team_B: row.team_B,
                                results: [{
                                    rank: row.rank,
                                    points_team_A: row.points_team_A !== null ? row.points_team_A : '-',
                                    points_team_B: row.points_team_B !== null ? row.points_team_B : '-'
                                }],
                                match_time: row.match_time,
                                field: row.field,
                                match_follower: row.match_follower,
                                label: row.label
                            });
                        } else {
                            //... or append result record to existing match entry
                            queryResult[queryResult.length - 1].results.push({
                                rank: row.rank,
                                points_team_A: row.points_team_A,
                                points_team_B: row.points_team_B
                            });
                        }
                    }
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function() {
                    callback(queryResult);
                }
            );
        });
        db.close();
    };

    return {
        find: function(params, cb) {
            if (params && params.id) {
                retrieveMatchesFromDB(params.id, function(resultObj) {
                    cb(null, resultObj);
                });
            }
        },
        findByIdAndUpdate: function(id, data) {
            return 'phase update ' + id;
        },
        save: function(data) {
            return 'phase save';
        },
        test: function() {
            return global.config.db;
        }
    };
};
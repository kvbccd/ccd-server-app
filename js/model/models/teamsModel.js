module.exports = teamsModel;

function teamsModel() {

    var retrieveTeamsFromDB = function(id, callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];

        db.serialize(function() {
            var selectStmt = "select t.id, t.name from team t" +
                " where t.type like 'real'" +
                " and t.tournament_id = $tournamentId" +
                " order by t.name";
            db.each(selectStmt, {
                    $tournamentId: id
                },
                function(err, row) {
                    queryResult.push({
                        id: row.id,
                        name: row.name
                    });
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function() {
                    callback(queryResult);
                }
            );
        });
        db.close();
    };

    return {
        find: function(params, cb) {
            if (params && params.id) {
                retrieveTeamsFromDB(params.id, function(resultObj) {
                    cb(null, resultObj);
                });
            }
        },
        findByIdAndUpdate: function(id, data) {
            return 'team update ' + id;
        },
        save: function(data) {
            return 'team save';
        },
        test: function() {
            return global.config.db;
        }
    };
};
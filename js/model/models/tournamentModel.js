module.exports = tournamentModel;

function tournamentModel() {

    var retrieveSingleFromDB = function(id, callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var check;
        var queryResult = [];

        // TODO: Utils zur Umformung und Formatierung in eigene Util-Datei auslagern:
        var formatdatestring = function(rawstring) {
            return rawstring.substring(8, 10) + '.' + rawstring.substring(5, 7) + '.' + rawstring.substring(0, 4)
        };

        db.serialize(function() {
            var selectStmt = "select id, name, phase_id, type" +
                " from tournament_phase" +
                " where tournament_id = $tournamentId" +
                " order by phase_id, id";
            db.each(selectStmt, {
                    $tournamentId: id
                },
                function(err, row) {
                    queryResult.push({
                        id: row.id,
                        name: row.name,
                        type: row.type,
                        phase_id: row.phase_id
                    });
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function() {
                    callback(queryResult);
                }
            );
        });
        db.close();
    };

    // TODO: DB-Zugriff in eigene Zwischenschicht auslagern:
    var retrieveAllFromDB = function(callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var check;
        var queryResult = [];

        // TODO: Utils zur Umformung und Formatierung in eigene Util-Datei auslagern:
        var formatdatestring = function(rawstring) {
            return rawstring.substring(8, 10) + '.' + rawstring.substring(5, 7) + '.' + rawstring.substring(0, 4)
        };

        db.serialize(function() {
            queryResult = [];
            db.each("SELECT id, tournament_mode, start_date, end_date, location FROM tournament",
                // row callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function(err, row) {
                    queryResult.push({
                        id: row.id,
                        type: row.tournament_mode,
                        start_date: '' /*formatdatestring(row.start_date)*/ ,
                        start_date: '' /*formatdatestring(row.end_date)*/ ,
                        location: '' /*row.location*/
                    });
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function(err, count) {
                    // console.log('send to caller '+count);
                    callback(queryResult);
                }
            );
        });
        db.close();
    };

    return {
        find: function(params, cb) {
            if (params && params.id) {
                retrieveSingleFromDB(params.id, function(resultObj) {
                    cb(null, resultObj);
                });
            } else {
                retrieveAllFromDB(function(resultObj) {
                    cb(null, resultObj);
                });
            }
        },
        findByIdAndUpdate: function(id, data) {
            return 'tournament update ' + id;
        },
        save: function(data) {
            return 'tournament save';
        },
        test: function() {
            return global.config.db;
        }
    };
};
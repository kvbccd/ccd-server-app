module.exports = phaseTableModel;

function phaseTableModel() {

    //Liest die Einträge aus der Tabelle tournament_phase_result.
    //Aufgrund der Sortierung spiegeln diese die darzustellende Gruppen-Tabelle wider.
    var retrieveTableFromDB = function(id, callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];
        var teamPosition = 1;
        //Lesen der berechneten Tabelle aus der Datenbank
        db.serialize(function() {
            var selectStmt = "select t.id, t.name, tpr.games_played, tpr.points, tpr.balls_scored, tpr.balls_conceded, tpr.rank" +
                " from tournament_phase_result tpr join team t on tpr.team_id = t.id" +
                " where tpr.tournament_phase_id = $tournamentId" +
                " order by tpr.points desc, tpr.rank desc, (tpr.balls_scored - tpr.balls_conceded) desc";
            db.each(selectStmt, {
                    $tournamentId: id
                },
                function(err, row) {
                    if (typeof row !== "undefined") {
                        if (typeof row !== "undefined") {
                            queryResult.push({
                                team_id: row.id,
                                position: teamPosition++,
                                team_name: row.name,
                                games_played: row.games_played,
                                points: row.points,
                                balls_scored: row.balls_scored,
                                balls_conceded: row.balls_conceded,
                                rank: row.rank
                            });
                        }
                    }
                },
                function() {
                    callback(queryResult);
                }
            );
        });

        db.close();
    };

    //Liest alle Ergebnisse einer Gruppe aus der Datenbank und erstellt eine Map.
    //In der Map werden für jedes Team die Anzahl der durchgeführten Spiele, die erzielten Punkte sowie die gewonnenen und verlorenen Bälle gespeichert.
    var retrieveResultsFromDB = function(data, callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];
        var resultMap = new Map();
        var lastMatchId = -1;
        var tournamentPhaseId = data.tournamentPhaseId;
        var tournamentId = data.tournamentId;

        var winPoints = 0;
        if (String(tournamentId) === String(1)) {
            winPoints = 2;
        } else {
            winPoints = 3;
        }

        var createMapObject = function(team_id, team_name, team_matches, team_points, team_balls_won, team_balls_lost) {
            var newValueObj = {
                team_id: team_id,
                team_name: team_name,
                team_matches: team_matches,
                team_points: team_points,
                team_balls_won: team_balls_won,
                team_balls_lost: team_balls_lost
            };
            return newValueObj;
        };

        db.serialize(function() {
            var selectStmt = "select m.id as match_id, m.teamA as team_A_id, t1.name as team_A, m.teamB as team_B_id, t2.name as team_B," +
                " m.match_time, m.field, r.rank, r.points_team_A, r.points_team_B," +
                " CASE WHEN r.points_team_A > r.points_team_B THEN $winPts WHEN r.points_team_B = r.points_team_A THEN 1 ELSE 0 END as PT_A," +
                " CASE WHEN r.points_team_B > r.points_team_A THEN $winPts WHEN r.points_team_A = r.points_team_B THEN 1 ELSE 0 END as PT_B" +
                " from match m" +
                " join team t1 on m.teamA = t1.ID" +
                " join team t2 on m.teamB = t2.ID" +
                " left join result r on m.id = r.match_id" +
                " where m.tournament_phase_id = $phaseId";
            db.each(selectStmt, {
                    $winPts: winPoints,
                    $phaseId: tournamentPhaseId
                },
                function(err, row) {
                    // rank ist null, falls noch kein Ergebnis eingetragen wurde
                    if (row.rank === null) {
                        //falls das Team noch nicht in der Map ist, erzeuge einen initialen Datensatz
                        if (!resultMap.has(row.team_A_id)) {
                            resultMap.set(row.team_A_id, createMapObject(row.team_A_id, row.team_A, 0, 0, 0, 0));
                        }
                        if (!resultMap.has(row.team_B_id)) {
                            resultMap.set(row.team_B_id, createMapObject(row.team_B_id, row.team_B, 0, 0, 0, 0));
                        }
                    } else {
                        //falls das Team noch nicht in der Map ist, erzeuge einen Datensatz mit dem gelesenen Ergebnis
                        if (!resultMap.has(row.team_A_id)) {
                            resultMap.set(row.team_A_id, createMapObject(row.team_A_id, row.team_A, 1, row.PT_A, row.points_team_A, row.points_team_B));
                        } else {
                            //falls das Team bereits in der Map ist, aktualisiere den Datensatz mit dem gelesenen Ergebnis
                            var teamObj = resultMap.get(row.team_A_id);
                            if (row.match_id !== lastMatchId) {
                                teamObj.team_matches = teamObj.team_matches + 1;
                            }
                            teamObj.team_points += row.PT_A;
                            teamObj.team_balls_won += row.points_team_A;
                            teamObj.team_balls_lost += row.points_team_B;
                            resultMap.set(row.team_A_id, teamObj);
                        }
                        //analog für des zweite Team des Ergebnisses
                        if (!resultMap.has(row.team_B_id)) {
                            resultMap.set(row.team_B_id, createMapObject(row.team_B_id, row.team_B, 1, row.PT_B, row.points_team_B, row.points_team_A));
                        } else {
                            var teamObj = resultMap.get(row.team_B_id);
                            if (row.match_id !== lastMatchId) {
                                teamObj.team_matches = teamObj.team_matches + 1;
                            }
                            teamObj.team_points += row.PT_B;
                            teamObj.team_balls_won += row.points_team_B;
                            teamObj.team_balls_lost += row.points_team_A;
                            resultMap.set(row.team_B_id, teamObj);
                        }
                    }
                    lastMatchId = row.match_id;
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function() {
                    // call insert function
                    callback(data.tournamentPhaseId, resultMap);
                }
            );
        });
        db.close();
    };

    //Speichert die Einträge aus der erstellten Map in die Datenbank.
    var saveGroupTable = function(tournamentPhaseId, resultMap) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);

        db.serialize(function() {
            //Erzeuge für jedes Team einen Update-Eintrag in der Datenbank
            resultMap.forEach(function(value, key) {
                var stmt = "update tournament_phase_result set games_played = " + value.team_matches +
                    ", points = " + value.team_points +
                    ", balls_scored = " + value.team_balls_won +
                    ", balls_conceded = " + value.team_balls_lost +
                    " where team_id = " + value.team_id +
                    " and tournament_phase_id = " + tournamentPhaseId;
                db.run(stmt);
            });
        });
        db.close();
    };

    //Führt ein Update auf die DB aus
    var executeUpdate = function(stmtArray) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        db.serialize(function() {
            for (var i = 0; i < stmtArray.length; i++) {
                db.run(stmtArray[i]);
            }
        });
        db.close();
    };

    //Speichert ein Ergebnis in der Datenbank
    var saveMatchResult = function(data) {

        //Falls keine Zahlen übergeben werden, so breche die Aktion ab
        if (isNaN(data.matchId) || isNaN(data.rank) || isNaN(data.pointsA) || isNaN(data.pointsB)) {
            return;
        }
        var stmtArray = [];
        var stmt = "insert or replace into result (match_id, rank, points_team_A, points_team_B) values (" +
            data.matchId + ", " + data.rank + ", " + data.pointsA + ", " + data.pointsB + ")";
        stmtArray.push(stmt);
        executeUpdate(stmtArray);
    };

    //Speichert den sog. Rank. Dies ist ein zusätzlicher Wert, um bei Punktegleichheit ein Team gegenüber eines anderen Teams zu bevorteilen
    var saveRank = function(data) {

        if (isNaN(data.teamId) || isNaN(data.rank)) {
            return;
        }
        var stmtArray = [];
        var stmt = "update tournament_phase_result set rank = " + data.rank + " where team_id = " + data.teamId;
        stmtArray.push(stmt);
        executeUpdate(stmtArray);
    };

    //holt sich das Ergebnis des Matches und befüllt in Abhängigkeit des erzielten Punkte das nächste Match
    var fillNextMatch = function(data) {

        if (isNaN(data.matchId)) {
            return;
        }
        var matchId = data.matchId;
        var stmtSelect = "select m.teamA, (select sum(points_team_A) from result where match_id = " + matchId +
            ") as points_team_a, m.teamB, (select sum(points_team_B) from result where match_id = " + matchId +
            ") as points_team_b, m.winner_match_id, m.loser_match_id, m.match_pos from match m where m.id = " + matchId;
        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        db.serialize(function() {
            db.each(stmtSelect,
                function(err, row) {
                    if (row.points_team_a !== null && row.points_team_b !== null) {
                        var stmtPrefix = "update match set team" + row.match_pos + " = ";
                        var stmtSuffix = " where id = ";
                        var stmtArray = [];
                        var stmtInsertWinner;
                        var stmtInsertLoser;
                        if (row.points_team_a === row.points_team_b) {
                            return;
                        } else if (row.points_team_a > row.points_team_b) {
                            stmtInsertWinner = stmtPrefix + row.teamA + stmtSuffix + row.winner_match_id;
                            stmtInsertLoser = stmtPrefix + row.teamB + stmtSuffix + row.loser_match_id;
                        } else {
                            stmtInsertWinner = stmtPrefix + row.teamB + stmtSuffix + row.winner_match_id;
                            stmtInsertLoser = stmtPrefix + row.teamA + stmtSuffix + row.loser_match_id;
                        }
                        stmtArray.push(stmtInsertWinner);
                        stmtArray.push(stmtInsertLoser);
                        executeUpdate(stmtArray);
                    } else {
                        return;
                    }
                },
                function() {});
        });
        db.close();
    };

    //schließt eine Phase ab und befüllt die nächsten Matches
    var concludePhase = function(data) {
        //Lese zuerst das Ergebnis der Gruppe
        retrieveTableFromDB(data.tournamentPhaseId, function(resultObj) {
            var sqlite3 = require('sqlite3').verbose();
            var db = new sqlite3.Database(global.config.db.sqlite.url);
            db.serialize(function() {
                var positionString = "";
                var teamIdArray = [];
                var stmtArray = [];
                var isStmtInArray = 0;
                var stmt = "select tpc.next_match_id, tpc.match_pos, tpc.position, tpc.tournament_phase_id from tournament_phase_conclusion tpc where tournament_phase_id = " + data.tournamentPhaseId + " and position in (";
                for (var i = 0; i < resultObj.length; i++) {
                    var item = resultObj[i];
                    //Zwischenspeichern des Wertes wg Callback-Hell
                    positionString += item.position + ",";
                    teamIdArray["" + item.position] = item.team_id;
                }
                db.each(stmt + positionString.slice(0, -1) + ")",
                    function(err, row) {
                        var teamId = teamIdArray[row.position];
                        var tournamentPhaseId = row.tournament_phase_id;
                        //Passe das Team (A oder B) im Match an
                        var stmtInsert = "update match set team" + row.match_pos + " = " + teamId + " where id = " + row.next_match_id;
                        stmtArray.push(stmtInsert);
                        //Hack für Fussball, falls Abschluss der Gruppenphase öfters geklickt wird
                        if (String(tournamentPhaseId) > String(20)) {
                            var nextPhaseTeam = [];
                            if (String(row.position) === String(3) && isStmtInArray < 3) {
                                nextPhaseTeam["21"] = "42";
                                nextPhaseTeam["22"] = "43";
                                nextPhaseTeam["23"] = "44";
                                nextPhaseTeam["24"] = "45";
                                stmtArray.push("update tournament_phase_result set team_id = " + teamId + " where tournament_phase_id = 26 and rowid = " + nextPhaseTeam[String(tournamentPhaseId)]);
                                isStmtInArray = 3;
                            } else if (String(row.position) === String(4) && isStmtInArray < 4) {
                                nextPhaseTeam["21"] = "46";
                                nextPhaseTeam["22"] = "48";
                                nextPhaseTeam["23"] = "49";
                                nextPhaseTeam["24"] = "50";
                                stmtArray.push("update tournament_phase_result set team_id = " + teamId + " where tournament_phase_id = 27 and rowid = " + nextPhaseTeam[String(tournamentPhaseId)]);
                                isStmtInArray = 4;
                            } else if (String(row.position) === String(5) && isStmtInArray < 5) {
                                nextPhaseTeam["21"] = "47";
                                stmtArray.push("update tournament_phase_result set team_id = " + teamId + " where tournament_phase_id = 27 and rowid = " + nextPhaseTeam[String(tournamentPhaseId)]);
                                isStmtInArray = 5;
                            }
                        }
                    },
                    function() {
                        executeUpdate(stmtArray);
                    });
            });
            db.close();
        });
    };

    return {
        find: function(params, cb) {
            if (params && params.id) {
                retrieveTableFromDB(params.id, function(resultObj) {
                    //console.log('get table for tournament phase ' + params.id + ': ' + resultObj);
                    cb(null, resultObj);
                });
            }
        },
        findByIdAndUpdate: function(id, data) {
            return 'phasetable update? ' + id;
        },
        save: function(data, cb) {
            if (data.action === 'calculateTable') {
                retrieveResultsFromDB(data, saveGroupTable);
            } else if (data.action === 'enterMatchResult') {
                saveMatchResult(data);
            } else if (data.action === 'setRank') {
                saveRank(data);
            } else if (data.action === 'fillNextMatch') {
                fillNextMatch(data);
            } else if (data.action === 'concludePhase') {
                if (String(data.phaseId) === String(1)) {
                    concludePhase(data);
                }
            } else {
                console.log('other action');
            }
            return 'phasetable save?';
        },
        test: function() {
            return global.config.db;
        }
    };
};
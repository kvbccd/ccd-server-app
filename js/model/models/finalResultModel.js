module.exports = finalResultModel;

function finalResultModel() {

    var getFinalResults = function(id, callback) {

        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];

        db.serialize(function() {
            var selectStmt = "select r.position, r.team_id, t.name" +
                " from tournament_result r inner join team t on t.id = r.team_id" +
                " where r.tournament_id = $tournamentId" +
                " order by r.position";
            db.each(selectStmt, {
                    $tournamentId: id
                },
                function(err, row) {
                    queryResult.push({
                        position: row.position,
                        id: row.team_id,
                        name: row.name
                    });
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function() {
                    callback(queryResult);
                }
            );
        });
        db.close();
    };

    return {
        find: function(params, cb) {
                if (params && params.id) {
                    getFinalResults(params.id, function(resultObj) {
                        cb(null, resultObj);
                    });
                }
            }
            //        ,
            //        findByIdAndUpdate: function(id, data) {
            //            return 'team update ' + id;
            //        },
            //        save: function(data) {
            //            return 'team save';
            //        },
            //        test: function() {
            //            return global.config.db;
            //        }
    };
};

// from the express site
module.exports = error;

function error(logger) {
    return {
        logErrors: function(err, req, res, next) {
            logger.error('logErrors caught this error!!!!');
            logger.error(err.stack);
            next(err);
        },

        // Where clientErrorHandler is defined as the following (note that the error is explicitly passed along to the next):
        clientErrorHandler: function(err, req, res, next) {
            if (req.xhr) {
                logger.error('clientErrorHandler caught this error!!!!');
                res.status(500).send({ error: 'Something blew up!' });
            } else {
                next(err);
            }
        },

        // The following errorHandler “catch-all” implementation may be defined as:
        errorHandler: function(err, req, res, next) {
            logger.error('errorHandler caught this error!!!!');
            res.status(500).send(err);
        }
    }
};
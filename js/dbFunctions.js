var dbFunctions = {
    retrieveMatchesFromDB: function(id, teamId, callback) {
        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];
        var strSQL = '';
        var strAnd = '';

        if (!isNaN(id)) {
            strSQL = "where m.tournament_phase_id = " + id;
            strAnd = " and ";
        } else {
            strSQL = "where ";
            strAnd = "";
        }

        if (!isNaN(teamId) && (teamId != null)) {
            strSQL = strSQL + strAnd + " (m.teamA = " + teamId + " or m.teamB = " + teamId + ")";
        }

        //console.log(strSQL);

        db.serialize(function() {
            db.each("select m.id as match_id, m.teamA as team_A_id, t1.name as team_A, m.teamB as team_B_id, t2.name as team_B, " +
                "m.match_time, m.field, r.rank, r.points_team_A, r.points_team_B, m.label from match m " +
                "join team t1 on m.teamA = t1.ID " +
                "join team t2 on m.teamB = t2.ID " +
                "left join result r on m.id = r.match_id " +
                strSQL,
                function(err, row) {
                    if (typeof row !== "undefined") {
                        // console.log("found match record for match " + row.match_id + ": "  + row.team_A + " - " + row.team_B + ", time: " + row.match_time);
                        var lastMatchId = queryResult.length > 0 ? queryResult[queryResult.length - 1].match_id : 0;
                        if (row.match_id !== lastMatchId) {
                            // open new match entry...
                            queryResult.push({
                                match_id: row.match_id,
                                team_A_id: row.team_A_id,
                                team_A: row.team_A,
                                team_B_id: row.team_B_id,
                                team_B: row.team_B,
                                results: [{
                                    rank: row.rank,
                                    points_team_A: row.points_team_A !== null ? row.points_team_A : '-',
                                    points_team_B: row.points_team_B !== null ? row.points_team_B : '-'
                                }],
                                match_time: row.match_time,
                                field: row.field,
                                label: row.label
                            });
                        } else {
                            //... or append result record to existing match entry
                            queryResult[queryResult.length - 1].results.push({
                                rank: row.rank,
                                points_team_A: row.points_team_A,
                                points_team_B: row.points_team_B
                            });
                        }
                    }
                },
                // completion callback (https://github.com/mapbox/node-sqlite3/wiki/API#databaseeachsql-param--callback-complete):
                function() {
                    callback(queryResult);
                }
            );
        });
        db.close();
    },
    //Liest die Einträge aus der Tabelle tournament_phase_result.
    //Aufgrund der Sortierung spiegeln diese die darzustellende Gruppen-Tabelle wider.
    retrieveTableFromDB: function(id, callback) {
        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];
        var teamPosition = 1;
        //Lesen der berechneten Tabelle aus der Datenbank
        db.serialize(function() {
            var selectStmt = "select t.id, t.name, tpr.games_played, tpr.points, tpr.balls_scored, tpr.balls_conceded"
                + " from tournament_phase_result tpr join team t on tpr.team_id = t.id"
                + " where tpr.tournament_phase_id = $tournamentPhaseId"
                + " order by tpr.points desc, tpr.rank desc, (tpr.balls_scored - tpr.balls_conceded) desc";
            db.each(selectStmt, {
                    $tournamentPhaseId: id
                },
                function(err, row) {
                    if (typeof row !== "undefined") {
                        queryResult.push({
                            team_id: row.id,
                            position: teamPosition++,
                            team_name: row.name,
                            games_played: row.games_played,
                            points: row.points,
                            balls_scored: row.balls_scored,
                            balls_conceded: row.balls_conceded
                        });
                    }
                },
                function() {
                    callback(queryResult);
                }
            );
        });

        db.close();
    },
    //Liest alle Spiele eines Tournaments sortiert nach Uhrzeit und Feld aus der DB
    retrieveAllMatchesFromDB: function(id, callback) {
        var sqlite3 = require('sqlite3').verbose();
        var db = new sqlite3.Database(global.config.db.sqlite.url);
        var queryResult = [];
        db.serialize(function() {
            var selectStmt = "select m.id as match_id"
                + ", replace(replace(replace(tp.name, 'Gruppe ', ''), 'Platzierungsrunde', 'PR'), ' bis ', '-') as phase_name_short"
                + ", tp.name as phase_name, tp.id as phase_id"
                + ", m.teamA as team_A_id, t1.name as team_A, m.teamB as team_B_id, t2.name as team_B"
                + ", m.match_time, m.field, r.rank, r.points_team_A, r.points_team_B"
                + " from match m join team t1 on m.teamA = t1.ID join team t2 on m.teamB = t2.ID"
                + " left join result r on m.id = r.match_id join tournament_phase tp on m.tournament_phase_id = tp.id"
                + " where tp.tournament_id = $tournamentId"
                + " order by m.match_time, field";
            db.each(selectStmt, {
                    $tournamentId: id
                },
                function(err, row) {
                    if (typeof row !== "undefined") {
                        var lastMatchId = queryResult.length > 0 ? queryResult[queryResult.length - 1].match_id : 0;
                        if (row.match_id !== lastMatchId) {
                            queryResult.push({
                                match_id: row.match_id,
                                phase_name_short: row.phase_name_short,
                                phase_name: row.phase_name,
                                phase_id: row.phase_id,
                                team_A_id: row.team_A_id,
                                team_A: row.team_A,
                                team_B_id: row.team_B_id,
                                team_B: row.team_B,
                                results: [{
                                    rank: row.rank,
                                    points_team_A: row.points_team_A !== null ? row.points_team_A : '-',
                                    points_team_B: row.points_team_B !== null ? row.points_team_B : '-'
                                }],
                                match_time: row.match_time,
                                field: row.field
                            });
                        } else {
                            //... or append result record to existing match entry
                            queryResult[queryResult.length - 1].results.push({
                                rank: row.rank,
                                points_team_A: row.points_team_A,
                                points_team_B: row.points_team_B
                            });
                        }
                    }
                },
                function() {
                    callback(queryResult);
                }
            );
        });
        db.close();
    }
};

module.exports = dbFunctions;
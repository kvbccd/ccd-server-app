var finalResultModel = require('../../model/models/finalResultModel')();
var genericModel = require('../../model/genericModel')(finalResultModel);

// router configuraton ========================================================
// import generic CRUD Router
var genericRoutes = require('../genericRoutes')(genericModel);

// model specific routes
//genericRoutes.get('/test/:test', function(req, res, next) { // FIXME
//	res.send(teamsModel.test());
//});

module.exports = genericRoutes;

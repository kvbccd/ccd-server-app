var phaseTableModel = require('../../model/models/phaseTableModel')();
var genericModel = require('../../model/genericModel')(phaseTableModel);

// router configuraton ========================================================
// import generic CRUD Router
var genericRoutes = require('../genericRoutes')(genericModel);

// model specific routes
//genericRoutes.get('/test/:test', function(req, res, next) { // FIXME
//	res.send(phaseTableModel.test());
//});

module.exports = genericRoutes;

var teamsModel = require('../../model/models/teamsModel')();
var genericModel = require('../../model/genericModel')(teamsModel);

// router configuraton ========================================================
// import generic CRUD Router
var genericRoutes = require('../genericRoutes')(genericModel);

// model specific routes
//genericRoutes.get('/test/:test', function(req, res, next) { // FIXME
//	res.send(teamsModel.test());
//});

module.exports = genericRoutes;

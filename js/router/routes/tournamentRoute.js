var tournamentModel = require('../../model/models/tournamentModel')();
var genericModel = require('../../model/genericModel')(tournamentModel);

// router configuraton ========================================================
// import generic CRUD Router
var genericRoutes = require('../genericRoutes')(genericModel);

// model specific routes
genericRoutes.get('/test/:test', function(req, res, next) { // FIXME
	res.send(tournamentModel.test());
});

module.exports = genericRoutes;
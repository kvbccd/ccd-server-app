var phaseModel = require('../../model/models/phaseModel')();
var genericModel = require('../../model/genericModel')(phaseModel);

// router configuraton ========================================================
// import generic CRUD Router
var genericRoutes = require('../genericRoutes')(genericModel);

// model specific routes
//genericRoutes.get('/test/:test', function(req, res, next) { // FIXME
//	res.send(phaseModel.test());
//});

module.exports = genericRoutes;
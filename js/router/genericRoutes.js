module.exports = genericRoute;

function genericRoute(Model) {
	var express = require('express');
	var router = express.Router();

	// router configuration ========================================================

	router.get('/', function(req, res, next) {
		Model.find(null,function (err, results) {
			if (err) {
                console.log ('got an error!' + err);
				return next(err);
			}
			else
			{
				res.send(results);
			}
		});
	});

    router.get('/:id', function (req, res, next) {
        // TODO: check that req.params.id exists and is set!!
        Model.findById(req.params.id, function (error, result) {
            if (error) {
                return next(error);
            }
            else
            {
                if(result) {
                    res.send(result);
                }
                else
                {
                    res.status(204).send();
                }
            }
        });
    });

    router.put('/', function(req, res, next) {
        // TODO: check that _id exists and is set!!
        Model.findByIdAndUpdate(req.body._id, req.body, function(err) {
            if(err) {
                return next(err);
            }
            else
            {
                res.status(201).send();
            }
        });
    });

    router.post('/', function(req, res, next) {
    	Model.save(req.body, function(err, obj) {
    		if (err) {
    			return next(err);
    		} else {
    			res.status(201).send(obj._id);
    		}
    	});

/*    	var model = new Model(req.body);
        model.save(function(err, obj) {
            if (err) {
                return next(err);
            }
            else
            {
                res.status(201).send(obj._id);
            }
        });
*/
    });

    router.post('/:id', function(req, res, next) {
      var model = new Model(req.body);
        model.save(function(err, obj) {
            if (err) {
                return next(err);
            }
            else
            {
                res.status(201).send(obj._id);
            }
        });
    });


    router.delete('/:id', function(req, res, next) {
        // TODO: check that req.params.id exists and is set!!
        Model.findByIdAndRemove(req.params.id, function(err) {
            if (err) {
                return next(err);
            }
            else
            {
                res.status(204).send();
            }
        });
    });
	return router;

}

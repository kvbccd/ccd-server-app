module.exports = function(app, logger) {
	app.use('/turnier', require('./routes/tournamentRoute.js'));
	app.use('/phase', require('./routes/phaseRoute.js'));
	app.use('/phasetable', require('./routes/phaseTableRoute.js'));
	app.use('/teams', require('./routes/teamsRoute.js'));
	app.use('/finalresults', require('./routes/finalResultRoute.js'));

	// error handlers
	var errorHandler = require('../error.js')(logger);
	app.use(errorHandler.logErrors);
	app.use(errorHandler.clientErrorHandler);
	app.use(errorHandler.errorHandler);

	// 404
	app.use(function(req, res, next) {
		res.status(404).send('Resource does not exist!')
	});
};

// modules =================================================
var express = require('express');
var log4js = require('log4js');

var app = express();
var auth = require('./auth')(app);
global.config = require('./js/config');
global.isLoggedIn = auth.isLoggedIn;

// configuration ===========================================

// this middleware will be executed for every request to the app
// test to see if user is set!
app.use(function(req, res, next) {
    // console.log('req.user', req.user, 'isAuthenticated:', req.isAuthenticated());
    // res.header("Content-Type",'application/json'); // für statische html tests auskommentiert !!!
    next();
});

// route middleware to handle index.html requests
function getIndex(req, res) {
    if (req.isAuthenticated()) {
        res.sendFile(__dirname + '/client/html' + '/adminIndex.html'); // refactor static directory to be global!
    } else {
        res.sendFile(__dirname + '/client/html' + '/index.html'); // refactor static directory to be global!
    }
}

app.get("/", getIndex);
app.get("/index.html", getIndex);
app.get("/adminIndex.html", getIndex);

// set static resource directory
// this has to be run after index.html handling middleware
app.use(express.static(__dirname + '/client/html'));

// setup logger
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('server.log'), 'server');

var logger = log4js.getLogger('server');
logger.setLevel('ALL');

// start app ===============================================
var server = app.listen(3000, function() {
    var port = server.address().port;
    console.log('Example app listening at port %s', port);
});

// this must be before the require router and socket
module.exports = server;

require('./js/websocket');

// router ==================================================
require('./js/router')(app, logger);

Im September 2016 waren die Teilnehmer des CCD in EPS dazu aufgerufen Vorschläge für die Umsetzung einer Anwendung mit Bezug zu mobilen Clients abzugeben. Im Anschluss daran fand eine Abstimmung statt, in der ein Vorschlag für eine Umsetzung im Rahmen der CCD-Termine ausgewählt wurde. Die meisten Stimmen erhielt dabei der Vorschlag eine Software für die Unterstützung des KV-Turniers für Fuß- und Volleyball im Juni 2017 zu erstellen.

Folgendes sollte existieren:
DB Name & Location: ../ccd_db/tournament.db